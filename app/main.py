def factorielle(n):
	if n==0:
		return 1
	elif n==1:
		return 1
	elif n<0:
		print("Erreur : impossible de retourner la valeur factorielle d'un nombre négatif")
		return -1
	else:
		return n*factorielle(n-1)

print("1 : ",factorielle(1))
print("0 : ",factorielle(0))
print("-4 : ",factorielle(-4))
print("4 : ",factorielle(4))
print("6 : ",factorielle(6))
